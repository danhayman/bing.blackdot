﻿using System.IO;

namespace Bing.BlackDot.Core
{
    public interface IScrapeParser
    {
        ResultSet Parse(Stream scrapings);
    }
}