﻿using System.Collections.Generic;

namespace Bing.BlackDot.Core
{
    public class ResultSet
    {
        public IList<Result> Results { get; set; }
        public bool IsPrevious { get; set; }
        public bool IsNext { get; set; }
    }
}