﻿namespace Bing.BlackDot.Core
{
    public interface IConfiguration
    {
        string UserAgent { get; }
        string QueryUrlPrefix { get; set; }
    }
}