﻿namespace Bing.BlackDot.Core
{
    public class ParserOptions
    {
        public string ResultXPath { get; set; }
        public string TitleXPath { get; set; }
        public string UrlXPath { get; set; }
        public string SummaryXPath { get; set; }
        public string NextPageXPath { get; set; }
        public string PreviousPageXPath { get; set; }
    }
}