﻿using System;
using System.Threading.Tasks;

namespace Bing.BlackDot.Core
{
    public class ScrapeCooridantor
    {
        private readonly IScraper scraper;
        private readonly IScrapeParser parser;

        public ScrapeCooridantor(IScraper scraper, IScrapeParser parser)
        {
            if (scraper == null) throw new ArgumentNullException(nameof(scraper));
            if (parser == null) throw new ArgumentNullException(nameof(parser));
            this.scraper = scraper;
            this.parser = parser;
        }

        public async Task<ResultSet> Scrape(string query, int firstResultIndex)
        {
            if (query == null) throw new ArgumentNullException(nameof(query));

            var scrapeTask = firstResultIndex > 0 ? scraper.Scrape(query, firstResultIndex) : scraper.Scrape(query);

            using (var scrape = await scrapeTask)
            {
                return parser.Parse(scrape);
            }
        }
    }
}