﻿namespace Bing.BlackDot.Core
{
    public class Result
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Summary { get; set; }
    }
}