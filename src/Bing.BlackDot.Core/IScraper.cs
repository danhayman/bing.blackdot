﻿using System.IO;
using System.Threading.Tasks;

namespace Bing.BlackDot.Core
{
    public interface IScraper
    {
        Task<Stream> Scrape(string query);
        Task<Stream> Scrape(string query, int firstResultIndex);
    }
}