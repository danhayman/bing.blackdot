﻿using System;
using System.Collections.Generic;
using System.IO;
using HtmlAgilityPack;
using Microsoft.Extensions.Options;

namespace Bing.BlackDot.Core
{
    public class ScrapeParser : IScrapeParser
    {
        private readonly Options options;

        public ScrapeParser(IOptions<Options> options)
        {
            this.options = options.Value;
        }

        public ResultSet Parse(Stream scrapings)
        {
            if (scrapings == null) throw new ArgumentNullException(nameof(scrapings));

            var document = new HtmlDocument();
            document.Load(scrapings);
            var documentDocumentNode = document.DocumentNode;
            var resultListItems = documentDocumentNode.SelectNodes(options.ParserOptions.ResultXPath);

            if (resultListItems == null)
            {
                return new ResultSet {Results = new List<Result>()};
            }

            var results = new List<Result>();
            foreach (var resultListItem in resultListItems)
            {
                var item = new Result();
                item.Title = resultListItem.SelectSingleNode(options.ParserOptions.TitleXPath)?.InnerText;
                item.Url = resultListItem.SelectSingleNode(options.ParserOptions.UrlXPath)?.Attributes["href"].Value;
                item.Summary = resultListItem.SelectSingleNode(options.ParserOptions.SummaryXPath)?.InnerHtml;
                results.Add(item);
            }

            var resultSet = new ResultSet();
            resultSet.Results = results;
            resultSet.IsPrevious = documentDocumentNode.SelectSingleNode(options.ParserOptions.PreviousPageXPath) != null;
            resultSet.IsNext = documentDocumentNode.SelectSingleNode(options.ParserOptions.NextPageXPath) != null;

            return resultSet;
        }
    }
}