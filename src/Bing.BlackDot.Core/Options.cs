﻿namespace Bing.BlackDot.Core
{
    public class Options
    {
        public string QueryUrlPrefix { get; set; }
        public string PagingUrlFragment { get; set; }
        public string UserAgent { get; set; }
        public ParserOptions ParserOptions { get; set; }
    }
}
