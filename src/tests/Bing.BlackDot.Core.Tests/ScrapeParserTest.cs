﻿using System;
using System.IO;
using Castle.Core.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using NUnit.Framework;

namespace Bing.BlackDot.Core.Tests
{
    [TestFixture]
    public class ScrapeParserTest
    {
        private Options options;

        [SetUp]
        public void SetUp()
        {
            var appSettings = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();
            options = new Options();
            appSettings.Bind(options);
        }

        [TestCase("sampleResponse1.html")]
        [TestCase("sampleResponse2.html")]        
        public void ParseParsesSamples(string fileName)
        {
            using (var fileStream = File.OpenRead(fileName))
            {
                var resultSet = new ScrapeParser(new OptionsWrapper<Options>(options)).Parse(fileStream);
                var results = resultSet.Results;
                foreach (var result in results)
                    Console.WriteLine($@"{result.Title}: {result.Url}");
                Assert.That(results.Count, Is.GreaterThan(0));
                results.ForEach(r =>
                {
                    Assert.That(r.Title, Is.Not.Null);
                    Assert.That(r.Url, Is.Not.Null);
                    Assert.That(r.Summary, Is.Not.Null);
                });

                Assert.That(resultSet.IsNext, Is.True);
                Assert.That(resultSet.IsPrevious, Is.False);
            }
        }
        
        [TestCase("sampleResponse3.html")]        
        public void ParseHandlesNoResults(string fileName)
        {
            using (var fileStream = File.OpenRead(fileName))
            {
                var resultSet = new ScrapeParser(new OptionsWrapper<Options>(options)).Parse(fileStream);
                var results = resultSet.Results;
                Assert.That(results.Count, Is.EqualTo(0));
            }
        }
    }
}