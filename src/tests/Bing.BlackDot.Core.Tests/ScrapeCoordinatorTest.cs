﻿using System.IO;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace Bing.BlackDot.Core.Tests
{
    [TestFixture]
    public class ScrapeCoordinatorTest
    {
        private Mock<IScraper> scraper;
        private Mock<IScrapeParser> parser;

        [SetUp]
        public void SetUp()
        {
            scraper = new Mock<IScraper>();
            parser = new Mock<IScrapeParser>();
        }

        [Test]
        public void ScrapeWithZeroFirstResultCallsOverloadWithoutFirstResultIndex()
        {
                Task.Run(async () => await new ScrapeCooridantor(scraper.Object, parser.Object).Scrape("hello", 0))
                    .GetAwaiter()
                    .GetResult();

            scraper.Verify(s => s.Scrape(It.Is<string>(q => q.Equals("hello"))));
        }

        [Test]
        public void ScrapeWithTenFirstResultCallsOverloadWithoutFirstResultIndex()
        {
                Task.Run(async () => await new ScrapeCooridantor(scraper.Object, parser.Object).Scrape("hello", 10))
                    .GetAwaiter()
                    .GetResult();

            scraper.Verify(s => s.Scrape(It.Is<string>(q => q.Equals("hello")), It.Is<int>(n => n == 10)));
        }

        [Test]
        public void ScrapeCallsParserParse()
        {
            Task.Run(async () => await new ScrapeCooridantor(scraper.Object, parser.Object).Scrape("hello", 0))
                    .GetAwaiter()
                    .GetResult();
            parser.Verify(p => p.Parse(It.IsAny<Stream>()));
        }
    }
}