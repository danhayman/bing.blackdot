﻿using System.IO;
using System.Threading.Tasks;
using Bing.BlackDot.Core;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace Bing.BlackDot.Integration.Tests
{
    [TestFixture]
    public class ScraperTest
    {
        [Test]
        public void ScrapeReturnsSomething()
        {
            var appSettings = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();
            var options = new Options();
            appSettings.Bind(options);

            using (var resultStream = Task.Run(async () => await new Scraper(new Microsoft.Extensions.Options.OptionsWrapper<Options>(options)).Scrape("blablabla")).GetAwaiter().GetResult())
            {
                using (var streamReader = new StreamReader(resultStream))
                {
                    var scrape = streamReader.ReadToEnd();
                    Assert.That(scrape.Length, Is.GreaterThan(0));
                    Assert.That(scrape, Contains.Substring("bing"));
                }
            }
        }
    }
}