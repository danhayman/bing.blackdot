﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Bing.BlackDot.Core;
using Microsoft.Extensions.Options;
using Options = Bing.BlackDot.Core.Options;

namespace Bing.BlackDot.Integration
{
    public class Scraper : IScraper
    {
        private readonly Options options;

        public Scraper(IOptions<Options> options)
        {
            this.options = options.Value;
            if (this.options.QueryUrlPrefix == null) throw new NullReferenceException("options.QueryUrlPrefix");
            if (this.options.UserAgent == null) throw new NullReferenceException("options.UserAgent");
        }

        public async Task<Stream> Scrape(string query)
        {
            if (query == null) throw new ArgumentNullException(nameof(query));

            var queryString = options.QueryUrlPrefix + WebUtility.UrlEncode(query);
            return await Scrape(new Uri(queryString));
        }

        public async Task<Stream> Scrape(string query, int firstResultIndex)
        {
            var queryString = options.QueryUrlPrefix + WebUtility.UrlEncode(query) + options.PagingUrlFragment + firstResultIndex;
            return await Scrape(new Uri(queryString));
        }

        private async Task<Stream> Scrape(Uri queryString)
        {
            var request = WebRequest.Create(queryString);

            ((HttpWebRequest) request).Headers[HttpRequestHeader.UserAgent] = options.UserAgent;
            var responseAsync = await request.GetResponseAsync();
            return responseAsync.GetResponseStream();
        }

        
    }
}
