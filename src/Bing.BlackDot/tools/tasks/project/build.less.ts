import * as gulp from 'gulp';
import * as less from 'gulp-less';
import { join } from 'path';

import Config from '../../config';

/**
 * This sample task copies all TypeScript files over to the appropiate `dist/dev|prod|test` directory, depending on the
 * current application environment.
 */
export = () => {
    return gulp.src(join(Config.APP_SRC, 'css/main.less'))
      .pipe(less())
        .pipe(gulp.dest(join(Config.APP_DEST, 'css')));
};
