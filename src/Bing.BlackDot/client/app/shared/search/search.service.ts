import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ResultSet } from '../index';
// import 'rxjs/add/operator/do';  // for debugging

@Injectable()
export class SearchService {

  constructor(private http: Http) {}

  search(query: string, first: number): Observable<ResultSet> {
      var pagingPart = '';
      if (first) {
          pagingPart = `&first=${first}`;
      }
      return this.http.get(`/api/bing?q=${encodeURIComponent(query)}${pagingPart}`)
          .map((res: Response) => res.json())
          //              .do(data => console.log('server data:', data))  // debug
          .catch(this.handleError);
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }  
}
