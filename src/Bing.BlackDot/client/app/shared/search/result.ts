﻿export class Result
{
    title: string;
    url: string;
    summary: string;
}