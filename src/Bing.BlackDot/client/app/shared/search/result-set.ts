﻿import { Result } from './index'

export class ResultSet {
    results: Result[];
    isPrevious: boolean;
    isNext: boolean;
}