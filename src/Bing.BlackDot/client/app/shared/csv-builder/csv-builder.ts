﻿import { Result } from '../index';

declare var Blob: {
    prototype: any;
    new (): any;
    new (data: any, mime: any): any;
};

declare var window: any;
declare var document: any;

export class CsvBuilder {

    static build(results: Result[]) {
        var str = '';

        for (var i = 0; i < results.length; i++) {
            var line = '';
            let result = results[i];
            for (var index in result) {
                if (result.hasOwnProperty(index)) {
                    if (line !== '') line += ',';

                    line += (<any>result)[index];
                }
            }

            str += line + '\r\n';
        }

        return str;
    }

    static download(data: string): any {
        let blob = new Blob([data], { type: 'text/csv' });
        var url = window.URL.createObjectURL(blob);
        var a: any = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('href', url);
        a.setAttribute('download', 'results.csv');        
        a.click();
        window.setTimeout(() => {
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);
        }, 100);
    }
}