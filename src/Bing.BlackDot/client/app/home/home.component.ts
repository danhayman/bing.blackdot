import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { Result, ResultSet, CsvBuilder, SearchService } from '../shared/index';
declare var window: any;

@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: 'home.component.html'
})

export class HomeComponent implements OnInit, OnDestroy {

    query: string;
    errorMessage: string;
    results: Result[];
    isPrevious: boolean;
    isNext: boolean;
    resultCount = 0;
    sub: any;
    
    constructor(public searchService: SearchService, private router: Router, private route: ActivatedRoute) {}

    ngOnInit() {
        this.sub = this.router.events.subscribe((event: any) => {
            if (event instanceof NavigationEnd) {
                this.actuallySearch();
            }
        });
    }

    search() {
        this.router.navigate(['/search'], { queryParams: { 'q': this.query } });     
    }

    downloadResults(event: any) {
        event.preventDefault();

        CsvBuilder.download(CsvBuilder.build(this.results));        
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    private actuallySearch() {
        let query = this.route.snapshot.queryParams['q'];
        let first = +this.route.snapshot.queryParams['first'];
        if (query) {
            this.query = query;
            this.searchService.search(query, first).subscribe(
                (resultSet: ResultSet) => {
                    this.results = resultSet.results;
                    this.isPrevious = resultSet.isPrevious;
                    this.isNext = resultSet.isNext;
                    window.scrollTo(0, 0);                                   
                },
                (error: any) => this.errorMessage = error
            );
        }
    }

    next(event: any) {
        event.preventDefault();
        if (this.isNext) {
            this.resultCount += this.results.length;
            this.router.navigate(['/search'], { queryParams: { 'q': this.query, 'first': this.resultCount } });
        }
    }

    previous(event: any) {
        event.preventDefault();
        if (this.isPrevious) {
            this.resultCount -= this.results.length;
            if (this.resultCount < 0) {
                this.resultCount = 0;
            }
            this.router.navigate(['/search'], { queryParams: { 'q': this.query, 'first': this.resultCount } });
        }
    }
}
