﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bing.BlackDot.Core;
using Microsoft.AspNetCore.Mvc;

namespace Bing.BlackDot.Controllers
{
    [Route("api/[controller]")]
    public class BingController : Controller
    {
        private readonly ScrapeCooridantor cooridantor;

        public BingController(ScrapeCooridantor cooridantor)
        {
            if (cooridantor == null) throw new ArgumentNullException(nameof(cooridantor));
            this.cooridantor = cooridantor;
        }

        [HttpGet]
        public async Task<ResultSet> Get([FromQuery(Name = "q")] string  query, [FromQuery(Name = "first")] int firstResultIndex)
        {
            if (query == null) throw new ArgumentNullException(nameof(query));
            return await cooridantor.Scrape(query, firstResultIndex);
        }
    }
}
