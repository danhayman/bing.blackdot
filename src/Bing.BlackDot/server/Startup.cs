﻿using System.IO;
using Bing.BlackDot.Core;
using Bing.BlackDot.Integration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace Bing.BlackDot
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var webRoot = env.IsDevelopment() ? @"dist\dev" : @"dist\prod";
            env.WebRootPath = Path.Combine(env.ContentRootPath, webRoot);

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddOptions();

            services.Configure<Options>(Configuration);
            services.AddTransient<ScrapeCooridantor>();
            services.AddTransient<IScraper, Scraper>();
            services.AddTransient<IScrapeParser, ScrapeParser>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.Use(async (context, next) =>
            {
                await next();

                if(context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/node_modules/") &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.GetFullPath(env.WebRootPath)),
            });
            if (env.IsDevelopment())
            {
                string libPath = Path.GetFullPath(Path.Combine(env.WebRootPath, @"..\..\node_modules\"));
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(libPath),
                    RequestPath = new PathString("/node_modules")
                });
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
